package Homework1;

import java.util.Random;
import java.util.Scanner;

public class Task3 {
    static int a;
    static int b;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        numbersFromKeyboard();
        ifAIsBiggerThanBChangePlaces();
        int[] elementList = get40RandomElementList();
        evenOrOdd(elementList);
        int sumOfEventsFromList = sumOfEvens(elementList);
        int sumOfOddsFromList = sumOfOdds(elementList);
        m5RandomNumbers(sumOfEventsFromList, sumOfOddsFromList);
    }


    public static void numbersFromKeyboard() {
        a = scanner.nextInt();
        b = scanner.nextInt();
    }

    public static void ifAIsBiggerThanBChangePlaces() {
        int helper = 0;
        if (a > b) {
            helper = b;
            b = a;
            a = helper;
        }
    }

    public static int[] get40RandomElementList() {
        int[] List = new int[40];
        Random rand = new Random();
        int max = 0;
        int min = 0;

        if (a > b) {
            max = a;
            min = b;
        } else {
            max = b;
            min = a;
        }
        for (int i = 0; 40 > i; i++) {
            List[i] = rand.nextInt((max - min) + 1) + min;
            System.out.println(i + " element is: " + List[i]);
        }
        return List;
    }

    public static void evenOrOdd(int[] forCheck) {
        int even = 0;
        int odd = 0;
        for (int j : forCheck) {
            if (j % 2 == 0) {
                even++;
            } else {
                odd++;
            }
        }
        System.out.println("There is: " + even + " even numbers And There is: " + odd + " odd numbers!");
    }

    public static int sumOfEvens(int[] checker) {
        int even = 0;
        int evenSum = 0;
        for (int j : checker) {
            if (j % 2 == 0) {
                even++;
                evenSum = evenSum + j;
            }
        }
        System.out.println("There is: " + even + " even its sum is : " + evenSum);
        return evenSum;
    }

    public static int sumOfOdds(int[] checker) {
        int odd = 0;
        int oddSum = 0;
        for (int j : checker) {
            if (j % 2 != 0) {
                odd++;
                oddSum = oddSum + j;
            }
        }
        System.out.println("There is: " + odd + " odds its sum is : " + oddSum);
        return oddSum;
    }

    public static void m5RandomNumbers(int a, int b) {
        Random rand = new Random();
        int[] List = new int[5];
        int max = 0;
        int min = 0;

        if (a > b) {
            max = a;
            min = b;
        } else {
            max = b;
            min = a;
        }
        for (int i = 0; 5 > i; i++) {
            List[i] = rand.nextInt((max - min) + 1) + min;
            System.out.println(i + " element is: " + List[i]);
        }
    }
}
