package Homework1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        C classC = new C();
        classC.method1();
        classC.method2();
        classC.method3();
        classC.method4();
        classC.method5();
        classC.method6();

        A classA = new A();
        classA.method1();
        classA.method2();
        classA.method3();

        B classB = new B();
        classB.method1();
        classB.method2();
    }
}

//Homework Part 1 //

class C {
    int a, b, c;
    Scanner scanner = new Scanner(System.in);

    void method1() {

        a = scanner.nextInt();
        b = scanner.nextInt();
        c = scanner.nextInt();

    }

    char method2() {

        String mA = String.valueOf(a);
        return mA.charAt(mA.length() - 1);

    }

    int method3() {
        String mB = String.valueOf(b);
        return Character.getNumericValue(mB.charAt(0));
    }

    int method4() {
        String mC = String.valueOf(c);
        int sum = 0;
        for (int i = 0; mC.length() > i; i++) {
            sum = sum + Character.getNumericValue(mC.charAt(i));
        }
        System.out.println(sum);
        return sum;
    }

    int method5() {
        System.out.println(method2() * method3());
        return method2() * method3();

    }

    void method6() {
        System.out.println(method3() + method5());
    }

}


//Homework Part 2//


class A {
    public int x;

    public A() {
        System.out.println("Hello");
    }

    void method1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number x: ");
        x = scanner.nextInt();
    }

    void method2() {
        System.out.println(x + 12);
    }

    void method3() {

        if (x % 2 == 0) {
            System.out.println("Number is even");
        } else {
            System.out.println("Number is odd");
        }
    }
}

class B extends A {

    int y;
    Scanner scanner = new Scanner(System.in);

    public void method1() {
        y = scanner.nextInt();
    }

    public void method2() {
        super.method1();
        System.out.println(x + y);
    }
}